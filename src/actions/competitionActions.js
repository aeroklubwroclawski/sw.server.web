import * as types from './actionTypes';
import competitionApi from '../api/competitionApi';
import * as eventActions from "./eventActions";
import * as schema from '../schema/schema';
import {normalize} from 'normalizr';

export function loadCompetitionsSuccess(payload) {
  return {type: types.LOAD_COMPETITIONS_SUCCESS, payload};
}

export function createCompetitionSuccess(competition) {
  return {type: types.CREATE_COMPETITION_SUCCESS, competition};
}

export function updateCompetitionSuccess(competition) {
  return {type: types.UPDATE_COMPETITION_SUCCESS, competition};
}

export function deleteCompetitionSuccess(competitionId) {
  return {type: types.DELETE_COMPETITION_SUCCESS, competitionId};
}

export function loadCompetitions() {
  return function (dispatch) {
    return competitionApi.getAllCompetitions()
      .then(payload => {
        const data = normalize(payload, [schema.competition]);
        dispatch(loadCompetitionsSuccess(data));
        return data.result;
      })
      .then(competitionIds => {
        competitionIds.map(id => dispatch(eventActions.loadEventsByCompetitionId(id)));
      })
      .catch(error => {
        throw error;
      });
  };
}

export function saveCompetition(competition) {
  return function (dispatch, getState) {
    (competition.id ?
      competitionApi.saveCompetition(competition) : competitionApi.createCompetition(competition))
      .then(savedCompetition => {
        if(savedCompetition) {
          dispatch(createCompetitionSuccess(savedCompetition));
        } else {
          competitionApi.getCompetitionById(competition.id)
            .then(result => {
              dispatch(updateCompetitionSuccess(result));
            });
        }
      }).catch(error => {
        throw error;
      });
  };
}

export function deleteCompetition(competition) {
  return (dispatch) =>{
    competitionApi.deleteCompetitionById(competition.id)
      .then(_ => {
        dispatch(deleteCompetitionSuccess(competition.id));
      });
  };
}


