import * as types from './actionTypes';
import eventApi from '../api/eventApi';
import * as schema from '../schema/schema';
import {normalize} from 'normalizr';

export function loadEventsSuccess(payload) {
  return {type: types.LOAD_EVENTS_SUCCESS, payload};
}

export function createEventSuccess(payload) {
  return {type: types.CREATE_EVENTS_SUCCESS, payload};
}

export function updateEventSuccess(payload) {
  return {type: types.UPDATE_EVENTS_SUCCESS, payload};
}

export function loadEventsByCompetitionId(competitionId) {
  return function (dispatch) {
    return eventApi.getCompetitionEvents(competitionId)
      .then(payload => {
        const data = normalize(payload.data, [schema.event]);
        dispatch(loadEventsSuccess(data));
        return data.result;
      }).catch(error => {
        throw error;
      });
  };
}

export function saveEvent(competitionId, event) {
  return function (dispatch, getState) {
    (event.id ?
      eventApi.saveEvent(competitionId, event) : eventApi.createEvent(competitionId, event))
      .then(savedEvent => {
        if(savedEvent) {
          dispatch(createEventSuccess(savedEvent));
        } else {
          eventApi.getEventById(event.id)
            .then(result => {
              dispatch(updateEventSuccess(result));
            });
        }
      }).catch(error => {
      throw error;
    });
  };
}
