import axios from 'axios';
import * as config from './apiConfig';

class CompetitionApi {
  static getAllCompetitions() {
    return axios
      .get(config.baseUrl + "/api/competitions")
      .then(response => response.data);
  }

  static getCompetitionById(competitionId) {
    return axios
      .get(config.baseUrl + '/api/competitions/' + competitionId)
      .then(response => response.data);
  }

  static createCompetition(competition) {
    const {name, description, bannerUrl} = competition;
    axios
      .post(config.baseUrl + '/api/competitions', {
        name,
        description,
        bannerUrl
      })
      .then(response => response.data);
  }

  static saveCompetition(competition) {
    const {name, description, bannerUrl} = competition;
    axios
      .put(config.baseUrl + '/api/competitions/' + competition.id,
        {
          name,
          description,
          bannerUrl
        });
  }

  static deleteCompetitionById(competitionId) {
    axios
      .delete(config.baseUrl + '/api/competitions/' + competitionId);
  }
}

export default CompetitionApi;
