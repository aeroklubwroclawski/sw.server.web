import axios from 'axios';
import * as config from './apiConfig';

class EventApi {
  static getCompetitionEvents(competitionId) {
    return axios.get(config.baseUrl + '/api/competitions/' + competitionId + '/events')
      .then(response => response.data);
  }

  static getEvent(competitionId, eventId) {
    return axios.get(config.baseUrl + '/api/competitions/' + competitionId + '/events/' + eventId)
      .then(response => response.data);
  }

  static createEvent(competitionId, event) {
    return axios.post(config.baseUrl + '/api/competitions/' + competitionId + '/events', event)
      .then(response => response);
  }

  static updateEvent(competitionId, event) {
    return axios.put(config.baseUrl + '/api/competitions/' + competitionId + '/events/' + event.id, event)
      .then(response => response);
  }

  static deleteEvent(competitionId, event) {
    return axios.delete(config.baseUrl + '/api/competitions/' + competitionId + '/events/' + event.id)
      .then(response => response);
  }
}

export default EventApi;

