import delay from './delay';

let competitions = [
  {
    name: "Competition 1",
    description: "Competition description",
    bannerUrl: "",
    events: []
  },
  {
    name: "Competition 2",
    description: "Competition description",
    bannerUrl: "",
    events: []
  }
];

class CompetitionApi {
  static getAllCompetitions() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign([], competitions));
      }, delay);
    });
  }

  static saveCompetition(competition) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if(competition.id) {
          const existingIndex = competitions.findIndex(c => c.id === competition.id);
          competitions.splice(existingIndex, 1, competition);
        } else {
          competition.id = new Date().getTime();
          competitions.push(competition);
        }

        competitions.push(competition);
        resolve(Object.assign({}, competition));
      }, delay);
    });
  }
}

export default CompetitionApi;
