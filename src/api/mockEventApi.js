import delay from './delay';

const events = [
  {
    id: 1,
    competitionId: 1,
    revision: 1,
    name: "Event name",
    description: "Description",
    startOpen: "2017-05-01 12:00:00"
  },
  {
    id: 2,
    competitionId: 1,
    revision: 2,
    name: "Other event name",
    description: "Description of other event",
    startOpen: "2017-05-02 10:00:00"
  },
  {
    id: 3,
    competitionId: 2,
    name: "Other event name",
    description: "Description of other event",
    startOpen: "2017-05-02 10:00:00"
  }
];

class EventApi {
  static getCompetitionEvents(competitionId){
    return new Promise((resolve, reject) => {
      setTimeout(() =>{
        const result = events.filter(e => e.competitionId == competitionId);

        resolve(Object.assign([], result));
      }, delay);
    });
  }
}

export default EventApi;
