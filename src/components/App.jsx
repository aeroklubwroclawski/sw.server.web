import React from 'react';
import PropTypes from 'prop-types';

import Header from "./common/Header";

class App extends React.Component {
  render() {
    return (
      <div>
        <Header />
        <div className="container">
          {this.props.routes}
        </div>
      </div>
    );
  }
}

App.propTypes = {
  routes: PropTypes.object.isRequired
};

export default App;
