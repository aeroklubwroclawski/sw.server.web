import React from 'react';
import PropTypes from 'prop-types';

const Panel = ({children, title}) => {
  return (
    <div className="panel panel-default">
      {title && <div className="panel-heading">{title}</div>}
      <div className="panel-body">
        {children}
      </div>
    </div>
  );
};

Panel.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string
};

export default Panel;
