import React from 'react';
import PropTypes from 'prop-types';
import TextInput from "../common/TextInput";

const CompetitionForm = ({competition, onChange, onSave, loading, errors}) => {
  return (
    <form>
      <h1>Manage competition</h1>
      <TextInput
        name="name"
        label="Name"
        value={competition.name}
        onChange={onChange}
        error={errors.name}
      />

      <TextInput
        name="description"
        label="Description"
        value={competition.description}
        onChange={onChange}
        error={errors.description}
      />

      <TextInput
        name="bannerUrl"
        label="Banner URL"
        value={competition.bannerUrl || ""}
        onChange={onChange}
        error={errors.bannerUrl}
      />

      <input
        type="submit"
        disabled={loading}
        value={loading ? "Saving..." : "Save"}
        className="btn btn-primary"
        onClick={onSave}/>
    </form>
  );
};

CompetitionForm.propTypes = {
  competition: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  errors: PropTypes.object
};

export default CompetitionForm;
