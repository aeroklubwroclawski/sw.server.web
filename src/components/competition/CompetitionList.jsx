import React from 'react';
import PropTypes from 'prop-types';
import CompetitionListItem from './CompetitionListItem';

const CompetitionList = ({competitions, onDeleteClick}) => {
  return (
    <table className="table">
      <thead>
      <tr>
        <th>Name</th>
        <th>Manage events</th>
        <th>Actions</th>
      </tr>
      </thead>
      <tbody>
      {competitions.map((item, i) =>
        <CompetitionListItem key={item.id} competition={item} onDeleteClick={onDeleteClick}/>
      )}
      </tbody>
    </table>
  );
};

CompetitionList.propTypes = {
  competitions: PropTypes.array.isRequired,
  onDeleteClick: PropTypes.func.isRequired
};

export default CompetitionList;
