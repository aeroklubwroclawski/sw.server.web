import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

const CompetitionListItem = ({competition, onDeleteClick}) => {
  const handleDeleteCompetition = () => {
    onDeleteClick(competition);
  };

  return (
    <tr>
      <td><Link to={'/competition/' + competition.id}>{competition.name}</Link></td>
      <td><Link to={'/competition/' + competition.id + '/events'}>Events</Link></td>
      <td>
        <button className="btn btn-danger" onClick={handleDeleteCompetition}><i className="fa fa-times"></i></button>
      </td>
    </tr>
  );
};

CompetitionListItem.propTypes = {
  competition: PropTypes.object.isRequired,
  onDeleteClick: PropTypes.func.isRequired
};

export default CompetitionListItem;
