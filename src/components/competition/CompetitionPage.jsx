import React from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';

import CompetitionStore from '../../store/CompetitionStore';
import CompetitionList from './CompetitionList';

@observer
class CompetitionPage extends React.Component {
  store = new CompetitionStore();

  constructor(props, context) {
    super(props, context);

    this.redirectToAddCompetitionPage = this.redirectToAddCompetitionPage.bind(this);
    this.onDeleteClick = this.onDeleteClick.bind(this);
  }

  componentDidMount() {
    this.store.fetchAll();
  }

  redirectToAddCompetitionPage() {
    this.context.router.history.push('/competition');
  }

  onDeleteClick(competition) {
    this.store.delete(competition);
  }

  render() {
    if(this.store.isLoading) {
      return (<div>Loading...</div>);
    }

    const {competitions} = this.store;

    return (
      <div>
        <h1>Competitions page</h1>
        <input
          type="submit"
          value="Add Competition"
          className="btn btn-primary"
          onClick={this.redirectToAddCompetitionPage}/>
        <CompetitionList competitions={competitions.slice()} onDeleteClick={this.onDeleteClick} />
      </div>
    );
  }
}

CompetitionPage.propTypes = {
};

CompetitionPage.contextTypes = {
  router: PropTypes.object
};

export default CompetitionPage;
