import React from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';

import Competition from '../../model/Competition';
import CompetitionForm from './CompetitionForm';

@observer
class ManageCompetitionPage extends React.Component {
  competition = new Competition();

  constructor(props, context) {
    super(props, context);

    this.updateCompetitionState = this.updateCompetitionState.bind(this);
    this.saveCompetition = this.saveCompetition.bind(this);
  }

  componentDidMount() {
    const competitionId = this.context.router.route.match.params.id;
    if (competitionId) {
      this.competition.loadById(competitionId);
    }
  }

  updateCompetitionState(event) {
    const field = event.target.name;
    this.competition[field] = event.target.value;
  }

  saveCompetition(event) {
    event.preventDefault();
    this.competition.save();
    this.context.router.history.push('/competitions');
  }

  render() {
    return (
      <CompetitionForm
        competition={Object.assign({}, this.competition)}
        onChange={this.updateCompetitionState}
        onSave={this.saveCompetition}
        errors={{}}/>
    );
  }
}

ManageCompetitionPage.propTypes = {};

ManageCompetitionPage.contextTypes = {
  router: PropTypes.object
};

export default ManageCompetitionPage;
