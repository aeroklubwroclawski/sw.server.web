import React from 'react';
import PropTypes from 'prop-types';

const CompetitionDetails = ({competition}) => {
  const {name, description, bannerUrl} = competition;
  return (
    <div className="media">
      <div className="media-left media-middle">
        <img className="media-object" src={bannerUrl ? bannerUrl : "http://placehold.it/100"} alt="" />
      </div>
      <div className="media-body">
        <div className="media-heading">
          <h4>{name}</h4>
          {description}
        </div>
      </div>
    </div>
  );
};

CompetitionDetails.propTypes = {
  competition: PropTypes.object.isRequired
};

export default CompetitionDetails;
