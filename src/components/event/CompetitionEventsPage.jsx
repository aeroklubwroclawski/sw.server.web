import React from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';

import Competition from '../../model/Competition';
import EventStore from '../../store/EventStore';
import CompetitionDetails from './CompetitionDetails';
import EventsList from './EventsList';

@observer
class CompetitionEventsPage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.onDeleteEvent = this.onDeleteEvent.bind(this);
    this.redirectToAddEvent = this.redirectToAddEvent.bind(this);
  }

  componentWillMount() {
    this.competition = new Competition();
    this.eventsStore = new EventStore();
  }

  componentDidMount() {
    const competitionId = this.context.router.route.match.params.id;
    if (competitionId) {
      this.competition.loadById(competitionId);
      this.eventsStore.fetchAll(competitionId);
    }
  }

  onDeleteEvent(event, eventToDelete) {
    event.preventDefault();
    const {id} = this.competition;
    this.eventsStore.deleteEvent(id, eventToDelete);
  }

  redirectToAddEvent() {
    this.context.router.history.push('/competition/' + this.competition.id + '/event');
  }

  render() {
    const {events} = this.eventsStore;

    return (
      <div>
        <h1>Competition events</h1>
        <CompetitionDetails competition={Object.assign({}, this.competition)}/>
        <button className="btn btn-primary" onClick={this.redirectToAddEvent}>Add event</button>
        <EventsList
          events={events.slice()}
          competitionId={this.competition.id}
          onDeleteEvent={this.onDeleteEvent}/>
      </div>
    );
  }
}

CompetitionEventsPage.propTypes = {
};

CompetitionEventsPage.contextTypes = {
  router: PropTypes.object
};

export default CompetitionEventsPage;
