import React from 'react';
import PropTypes from 'prop-types';

class CoordinateCalculator extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      degrees: 0.0,
      minutes: 0.0,
      seconds: 0.0,
      coordinates: 0.0
    };

    this.onUpdate = this.onUpdate.bind(this);
    this.updateCoordinates = this.updateCoordinates.bind(this);
  }

  updateCoordinates() {
    this.setState((prevState, props)=>({
      coordinates: (prevState.degrees) + (prevState.minutes / 60.0) + (prevState.seconds / 3600.0)
    }));
  }

  onUpdate(event) {
    event.preventDefault();

    this.setState({
      [event.target.name]: parseFloat(event.target.value)
    });
  }

  render() {
    return (
      <div className="calculator" data-spy="affix" data-offset-top="70px">
        <div className="form-inline">
          <div className="form-group">
            <input type="text" className="form-control coordinate" name="degrees" value={this.state.degrees} onChange={this.onUpdate} onBlur={this.updateCoordinates}/>
            <span>deg</span>
            <input type="text" className="form-control coordinate" name="minutes" value={this.state.minutes} onChange={this.onUpdate} onBlur={this.updateCoordinates}/>
            <span>min</span>
            <input type="text" className="form-control coordinate" name="seconds" value={this.state.seconds} onChange={this.onUpdate} onBlur={this.updateCoordinates}/>
            <span>sec</span>
          </div>
        </div>

        <input type="text" disabled="true" className="form-control" value={this.state.coordinates}/>
      </div>
    );
  }
}

CoordinateCalculator.propTypes = {};

export default CoordinateCalculator;
