import React from 'react';
import PropTypes from 'prop-types';

import Panel from '../common/Panel';
import TextInput from '../common/TextInput';

const EventForm = ({children, event, onChange, onSave, loading, errors}) => {
  return (
    <form>
      <h1>Event form</h1>
      <Panel title="Event details">
        <TextInput
          name="name"
          label="Name"
          value={event.name}
          onChange={onChange}
          error={errors}
        />
        <TextInput
          name="description"
          label="Description"
          value={event.description}
          onChange={onChange}
          error={errors}
        />
        <TextInput
          name="bannerUrl"
          label="Banner URL"
          value={event.bannerUrl}
          onChange={onChange}
          error={errors}
        />
        <TextInput
          name="startOpen"
          label="Start open"
          value={event.startOpen}
          onChange={onChange}
          error={errors}
        />
      </Panel>

      {children}

      <input
        type="submit"
        disabled={loading}
        value={loading ? "Saving..." : "Save"}
        className="btn btn-primary"
        onClick={onSave}/>
    </form>
  );
};

EventForm.propTypes = {
  children: PropTypes.node,
  event: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  errors: PropTypes.object
};

export default EventForm;
