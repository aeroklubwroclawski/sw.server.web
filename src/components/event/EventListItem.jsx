import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

const EventListItem = ({event, competitionId, onDeleteEvent}) => {
  const onDelete = (evt) => {
    onDeleteEvent(evt, event);
  };

  return (
    <tr>
      <td><Link to={"/competition/" + competitionId + "/event/" + event.id}>{event.name}</Link></td>
      <td>{event.description}</td>
      <td>{event.startOpen}</td>
      <td><button className="btn btn-danger btn-sm" onClick={onDelete}><i className="fa fa-times"/></button></td>
    </tr>
  );
};

EventListItem.propTypes = {
  event: PropTypes.object.isRequired,
  competitionId: PropTypes.string.isRequired,
  onDeleteEvent: PropTypes.func.isRequired
};

export default EventListItem;
