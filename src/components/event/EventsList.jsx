import React from 'react';
import PropTypes from 'prop-types';
import EventListItem from './EventListItem';

const EventsList = ({events, competitionId, onDeleteEvent}) => {
  return (
    <table className="table">
      <thead>
      <tr>
        <th>Name</th>
        <th>Description</th>
        <th>Start open</th>
        <th></th>
      </tr>
      </thead>
      <tbody>
      {events.map((item, i) =>
        <EventListItem key={i} event={item} competitionId={competitionId} onDeleteEvent={onDeleteEvent}/>
      )}
      </tbody>
    </table>
  );
};

EventsList.propTypes = {
  events: PropTypes.array.isRequired,
  competitionId: PropTypes.string.isRequired,
  onDeleteEvent: PropTypes.func.isRequired
};

export default EventsList;
