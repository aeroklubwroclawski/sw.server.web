import React from 'react';
import PropTypes from 'prop-types';
import {observer} from 'mobx-react';

import Event from '../../model/Event';
import EventForm from './EventForm';
import TaskForm from './TaskForm';
import TrackForm from './TrackForm';
import CoordinateCalculator from './CoordinateCalculator';

@observer
class ManageEventPage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.updateEventState = this.updateEventState.bind(this);
    this.updateTaskState = this.updateTaskState.bind(this);
    this.updateTurnpointState = this.updateTurnpointState.bind(this);
    this.updateTrackState = this.updateTrackState.bind(this);
    this.addTurnpoint = this.addTurnpoint.bind(this);
    this.deleteTurnpoint = this.deleteTurnpoint.bind(this);
    this.addTrack = this.addTrack.bind(this);
    this.deleteTrack = this.deleteTrack.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  componentWillMount() {
    const competitionId = this.context.router.route.match.params.id;
    this.event = new Event(competitionId);
  }

  componentDidMount() {
    const competitionId = this.context.router.route.match.params.id;
    const eventId = this.context.router.route.match.params.eventId;

    if (eventId) {
      this.event.load(competitionId, eventId);
    }
  }

  updateEventState(event) {
    const field = event.target.name;
    this.event[field] = event.target.value;
  }

  updateTaskState(event) {
    const field = event.target.name;
    this.event.task[field] = event.target.value;
  }

  updateTurnpointState(event, turnpointId) {
    const field = event.target.name;
    let turnpoint = this.event.task.turnpoints.filter(x => x.id === turnpointId)[0];
    this.event.task.updateTurnpoint({
      ...turnpoint,
      [field]: event.target.value
    });
  }

  updateTrackState(event, trackpointId) {
    const field = event.target.name;
    let trackpoint = this.event.tracks.filter(x => x.id === trackpointId)[0];
    this.event.updateTrack({
      ...trackpoint,
      [field]: event.target.value
    });
  }

  addTurnpoint(event) {
    event.preventDefault();
    this.event.task.addTurnpoint();
  }

  deleteTurnpoint(event, turnpoint) {
    event.preventDefault();
    this.event.task.deleteTurnpoint(turnpoint);
  }

  addTrack(event) {
    event.preventDefault();
    this.event.addTrack();
  }

  deleteTrack(event, track) {
    event.preventDefault();
    this.event.deleteTrack(track);
  }

  onSave(event) {
    event.preventDefault();
    this.event.save();

    const competitionId = this.context.router.route.match.params.id;
    this.context.router.history.push('/competition/' + competitionId + '/events')
  }

  render() {
    return (
      <div>
        <CoordinateCalculator/>
        <EventForm
          event={Object.assign({}, this.event)}
          onChange={this.updateEventState}
          onAddTurnpoint={this.addTurnpoint}
          onSave={this.onSave}>
          <TaskForm
            task={Object.assign({}, this.event.task)}
            turnpoints={this.event.task.turnpoints.slice()}
            onChange={this.updateTaskState}
            onTurnpointChange={this.updateTurnpointState}
            onAddTurnpoint={this.addTurnpoint}
            onDeleteTurnpoint={this.deleteTurnpoint}/>
          <TrackForm
            tracks={this.event.tracks.slice()}
            onAddTrack={this.addTrack}
            onDeleteTrack={this.deleteTrack}
            onTrackChange={this.updateTrackState}/>
        </EventForm>
      </div>
    );
  }
}

ManageEventPage.propTypes = {};

ManageEventPage.contextTypes = {
  router: PropTypes.object
};

export default ManageEventPage;
