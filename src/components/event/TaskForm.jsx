import React from 'react';
import PropTypes from 'prop-types';

import Panel from '../common/Panel';
import TextInput from '../common/TextInput';
import TurnpointForm from './TurnpointForm';

const TaskForm = ({task, turnpoints, onChange, onAddTurnpoint, onDeleteTurnpoint, onTurnpointChange}) => {
  return (
    <Panel title="Task">
      <TextInput
        name="name"
        label="Name"
        onChange={onChange}
        value={task.name}/>

      <TextInput
        name="startOpen"
        label="Start open"
        onChange={onChange}
        value={task.startOpen}/>

      <TurnpointForm
        turnpoints={turnpoints}
        onAddTurnpoint={onAddTurnpoint}
        onTurnpointChange={onTurnpointChange}
        onDeleteTurnpoint={onDeleteTurnpoint}/>
    </Panel>
  );
};

TaskForm.propTypes = {
  task: PropTypes.object.isRequired,
  turnpoints: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  onAddTurnpoint: PropTypes.func.isRequired,
  onDeleteTurnpoint: PropTypes.func.isRequired,
  onTurnpointChange: PropTypes.func.isRequired
};

export default TaskForm;
