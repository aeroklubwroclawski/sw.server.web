import React from 'react';
import PropTypes from 'prop-types';

import Panel from '../common/Panel';
import TrackList from './TrackList';

const TrackForm = ({tracks, onAddTrack, onDeleteTrack, onTrackChange}) => {
    return (
      <Panel title="Tracks">
        <button className="btn btn-primary" onClick={onAddTrack}>Add trackpoint</button>
        <TrackList tracks={tracks} onTrackChange={onTrackChange} onDeleteTrack={onDeleteTrack}/>
      </Panel>
    );
};

TrackForm.propTypes = {
  tracks: PropTypes.array.isRequired,
  onAddTrack: PropTypes.func.isRequired,
  onDeleteTrack: PropTypes.func.isRequired,
  onTrackChange: PropTypes.func.isRequired
};

export default TrackForm;
