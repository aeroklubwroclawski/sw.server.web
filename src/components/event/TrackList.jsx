import React from 'react';
import PropTypes from 'prop-types';

import TrackListItem from './TrackListItem';

const TrackList = ({tracks, onTrackChange, onDeleteTrack}) => {
  return (
    <table className="table">
      <thead>
      <tr>
        <th>Track ID</th>
        <th>Pilot name</th>
        <th>Competition ID</th>
        <th>Country</th>
        <th>Aircraft</th>
        <th>Portrait URL</th>
        <th>Registration</th>
        <th></th>
      </tr>
      </thead>
      <tbody>
      {tracks && tracks.map((item, i) =>
        <TrackListItem key={i} track={item} onTrackpointChange={onTrackChange} onDeleteTrack={onDeleteTrack}/>
      )}
      </tbody>
    </table>
  );
};

TrackList.propTypes = {
  tracks: PropTypes.array,
  onTrackChange: PropTypes.func.isRequired,
  onDeleteTrack: PropTypes.func.isRequired
};

export default TrackList;
