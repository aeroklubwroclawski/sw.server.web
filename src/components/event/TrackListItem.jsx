import React from 'react';
import PropTypes from 'prop-types';

import TableTextInput from '../common/TableTextInput';
import TableSelectInput from '../common/TableSelectInput';

const TrackListItem = ({track, onTrackpointChange, onDeleteTrack}) => {
  const onChange = (event) => {
    onTrackpointChange(event, track.id);
  };

  const onDelete = (event) => {
    onDeleteTrack(event, track);
  };

  const modelVariants = [
    {value: "Bramar", text: "Bramar"},
    {value: "Hasco-lek", text: "Hasco-lek"},
    {value: "Holmes-place", text: "Holmes-place"},
    {value: "Hydropolis", text: "Hydropolis"},
    {value: "It-elite", text: "It-elite"},
    {value: "Roleski", text: "Roleski"},
    {value: "Triada-dom", text: "Triada-dom"}
  ];

  return (
    <tr>
      <td><TableTextInput name="trackId" onChange={onChange} value={track.trackId}/></td>
      <td><TableTextInput name="pilotName" onChange={onChange} value={track.pilotName}/></td>
      <td><TableTextInput name="competitionId" onChange={onChange} value={track.competitionId}/></td>
      <td><TableTextInput name="country" onChange={onChange} value={track.country}/></td>
      <td><TableTextInput name="aircraft" onChange={onChange} value={track.aircraft}/></td>
      <td><TableTextInput name="portraitUrl" onChange={onChange} value={track.portraitUrl}/></td>
      <td><TableTextInput name="registration" onChange={onChange} value={track.registration}/></td>
      <td><TableSelectInput name="modelVariant" onChange={onChange} value={track.modelVariant} options={modelVariants}/></td>
      <td><button className="btn btn-danger btn-sm" onClick={onDelete}><i className="fa fa-times"/></button></td>
    </tr>
  );
};

TrackListItem.propTypes = {
  track: PropTypes.object.isRequired,
  onTrackpointChange: PropTypes.func.isRequired,
  onDeleteTrack: PropTypes.func.isRequired
};

export default TrackListItem;
