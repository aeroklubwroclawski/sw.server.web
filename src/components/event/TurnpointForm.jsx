import React from 'react';
import PropTypes from 'prop-types';

import Panel from '../common/Panel';
import TurnpointList from './TurnpointList';

const TurnpointForm = ({turnpoints, onAddTurnpoint, onDeleteTurnpoint, onTurnpointChange}) => {
  return (
    <Panel title="Turnpoints">
      <button className="btn btn-primary" onClick={onAddTurnpoint}>Add turnpoint</button>
      <TurnpointList turnpoints={turnpoints} onTurnpointChange={onTurnpointChange}
                     onDeleteTurnpoint={onDeleteTurnpoint}/>
    </Panel>
  );
};

TurnpointForm.propTypes = {
  turnpoints: PropTypes.array.isRequired,
  onAddTurnpoint: PropTypes.func.isRequired,
  onDeleteTurnpoint: PropTypes.func.isRequired,
  onTurnpointChange: PropTypes.func.isRequired
};

export default TurnpointForm;
