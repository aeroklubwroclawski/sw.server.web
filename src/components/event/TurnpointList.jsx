import React from 'react';
import PropTypes from 'prop-types';

import TurnPointListItem from './TurnpointListItem';

const TurnpointList = ({turnpoints, onTurnpointChange, onDeleteTurnpoint}) => {
  return (
    <table className="table">
      <thead>
      <tr>
        <th>Name</th>
        <th>Type</th>
        <th>Latitude</th>
        <th>Longitude</th>
        <th>Min Alt</th>
        <th>Max Alt</th>
        <th>Observation zone</th>
        <th>Radius</th>
        <th className="col-xs-1">Trigger</th>
        <th>Cone incline</th>
        <th></th>
      </tr>
      </thead>
      <tbody>
      {turnpoints.map((t) => <TurnPointListItem key={t.id} turnpoint={t} onTurnpointChange={onTurnpointChange} onDeleteTurnpoint={onDeleteTurnpoint}/>)}
      </tbody>
    </table>
  );
};

TurnpointList.propTypes = {
  turnpoints: PropTypes.array.isRequired,
  onTurnpointChange: PropTypes.func.isRequired,
  onDeleteTurnpoint: PropTypes.func.isRequired
};

export default TurnpointList;
