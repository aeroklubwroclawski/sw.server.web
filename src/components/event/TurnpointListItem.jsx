import React from 'react';
import PropTypes from 'prop-types';
import TableTextInput from '../common/TableTextInput';
import TableSelectInput from '../common/TableSelectInput';

const TurnpointListItem = ({turnpoint, onTurnpointChange, onDeleteTurnpoint}) => {
  const onChange = (event) => {
    onTurnpointChange(event, turnpoint.id);
  };

  const onDelete = (event) => {
    onDeleteTurnpoint(event, turnpoint);
  };

  const typeOptions = [
    {value: "Takeoff", text: "Take off"},
    {value: "Start", text: "Start"},
    {value: "StartOfSpeedSection", text: "Start of speed section"},
    {value: "Turnpoint", text: "Turnpoint"},
    {value: "EndOfSpeedSection", text: "End of speed section"},
    {value: "Finish", text: "Finish"},
    {value: "Landing", text: "Landing"},
  ];

  const observationZoneOptions = [
    {value: "Cylinder", text: "Cylinder"},
    {value: "Sector", text: "Sector"},
    {value: "Line", text: "Line"},
    {value: "Cone", text: "Cone"}
  ];

  const triggerOptions = [
    {value: "Enter", text: "Enter"},
    {value: "Exit", text: "Exit"}
  ];

  return (
    <tr>
      <td><TableTextInput name="name" onChange={onChange} value={turnpoint.name}/></td>
      <td><TableSelectInput name="type" onChange={onChange} value={turnpoint.type} options={typeOptions}/></td>
      <td><TableTextInput name="latitude" onChange={onChange} value={turnpoint.latitude}/></td>
      <td><TableTextInput name="longitude" onChange={onChange} value={turnpoint.longitude}/></td>
      <td><TableTextInput name="minAltitude" onChange={onChange} value={turnpoint.minAltitude}/></td>
      <td><TableTextInput name="maxAltitude" onChange={onChange} value={turnpoint.maxAltitude}/></td>
      <td><TableSelectInput name="observationZone" onChange={onChange} value={turnpoint.observationZone} options={observationZoneOptions}/></td>
      <td><TableTextInput name="radius" onChange={onChange} value={turnpoint.radius}/></td>
      <td><TableSelectInput name="trigger" onChange={onChange} value={turnpoint.trigger} options={triggerOptions}/></td>
      <td><TableTextInput name="coneIncline" onChange={onChange} value={turnpoint.coneIncline}/></td>
      <td><button className="btn btn-danger btn-sm" onClick={onDelete}><i className="fa fa-times"/></button></td>
    </tr>
  );
};

TurnpointListItem.propTypes = {
  turnpoint: PropTypes.object.isRequired,
  onTurnpointChange: PropTypes.func.isRequired,
  onDeleteTurnpoint: PropTypes.func.isRequired
};

export default TurnpointListItem;
