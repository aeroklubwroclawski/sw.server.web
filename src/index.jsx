import 'babel-polyfill';
import React from 'react';
import {render} from 'react-dom';
import {HashRouter} from 'react-router-dom';
import createHistory from 'history/createBrowserHistory';
import 'jquery';

import routes from "./routes";
import App from "./components/App";

const browserHistory = createHistory();

render((
    <HashRouter history={browserHistory}>
      <App routes={routes}/>
    </HashRouter>
  ),
  document.getElementById('app')
);
