import {action, autorun, observable, toJS} from 'mobx';
import CompetitionApi from '../api/competitionApi';

export default class Competition {
  @observable id = "";
  @observable name = "";
  @observable description = "";
  @observable bannerUrl = "";

  constructor() {
    autorun(() => console.log(toJS(this)));
  }

  @action loadById(id) {
    CompetitionApi.getCompetitionById(id)
      .then(data => this.fromApi(data));
  }

  @action fromApi(data) {
    this.id = data.id;
    this.name = data.name;
    this.description = data.description;
    this.bannerUrl = data.bannerUrl;
  }

  save() {
    if(this.id) {
      CompetitionApi.saveCompetition(this);
    } else {
      CompetitionApi.createCompetition(this);
    }
  }
}
