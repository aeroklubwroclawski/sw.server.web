import {action, autorun, observable, toJS} from 'mobx';

import Task from './Task';
import Trackpoint from './Trackpoint';
import EventApi from '../api/eventApi';

class Event {
  competitionId;
  @observable id = "";
  @observable name = "";
  @observable description = "";
  @observable bannerUrl = "";
  @observable eventRevision = "";
  @observable startOpen = "";
  @observable task = new Task();
  @observable tracks = [];

  constructor(competitionId) {
    this.competitionId = competitionId;
    autorun(() => console.log(toJS(this)));
  }

  @action addTrack() {
    this.tracks.push(new Trackpoint());
  }

  @action updateTrack(track) {
    const oldTrack = this.tracks.filter(x => x.id === track.id)[0];
    const index = this.tracks.indexOf(oldTrack);
    this.tracks.splice(index, 1, track);
  }

  @action deleteTrack(track) {
    const index = this.tracks.indexOf(track);
    if (index >= 0) {
      this.tracks.splice(index, 1);
    }
  }

  @action load(competitionId, eventId) {
    EventApi.getEvent(competitionId, eventId)
      .then(data => this.fromApi(data));
  }

  @action save() {
    if (this.id) {
      EventApi.updateEvent(this.competitionId, this);
    } else {
      EventApi.createEvent(this.competitionId, this);
    }
  }

  @action fromApi(data) {
    this.id = data.id;
    this.name = data.name;
    this.description = data.description;
    this.bannerUrl = data.bannerUrl;
    this.eventRevision = data.eventRevision;
    this.startOpen = data.startOpen;
    this.task.fromApi(data.task);

    data.tracks.map(t => {
      let trackpoint = new Trackpoint();
      trackpoint.fromApi(t);
      this.tracks.push(trackpoint);
    });
  }
}

export default Event;
