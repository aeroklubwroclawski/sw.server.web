import {action, autorun, observable, toJS} from 'mobx';
import Turnpoint from './Turnpoint';

class Task {
  @observable name = "";
  @observable startOpen = "";
  @observable turnpoints = [];

  constructor() {
    autorun(() => console.log(toJS(this)));
  }

  @action addTurnpoint() {
    this.turnpoints.push(new Turnpoint());
  }

  @action updateTurnpoint(turnpoint) {
    const oldTurnpoint = this.turnpoints.filter(x => x.id === turnpoint.id)[0];
    const index = this.turnpoints.indexOf(oldTurnpoint);
    this.turnpoints.splice(index, 1, turnpoint);
  }

  @action deleteTurnpoint(turnpoint) {
    const index = this.turnpoints.indexOf(turnpoint);
    if (index >= 0) {
      this.turnpoints.splice(index, 1);
    }
  }

  @action fromApi(data) {
    this.name = data.name;
    this.startOpen = data.startOpen;

    data.turnpoints.map((t) => {
      let turnpoint = new Turnpoint();
      turnpoint.fromApi(t);
      this.turnpoints.push(turnpoint);
    });
  }
}

export default Task;
