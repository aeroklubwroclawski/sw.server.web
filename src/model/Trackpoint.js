import {action, autorun, observable, toJS} from 'mobx';
import {guid} from '../util';

export default class Trackpoint {
  id;
  @observable trackId = "";
  @observable pilotName = "";
  @observable competitionId = "";
  @observable country = "";
  @observable aircraft = "";
  @observable portraitUrl = "";
  @observable registration = "";
  @observable modelVariant = "";

  constructor() {
    this.id = guid();
    autorun(() => console.log(toJS(this)));
  }

  @action fromApi(data) {
    this.trackId = data.trackId || "";
    this.pilotName = data.pilotName || "";
    this.competitionId = data.competitionId || "";
    this.country = data.country || "";
    this.aircraft = data.aircraft || "";
    this.portraitUrl = data.portraitUrl || "";
    this.registration = data.registration || "";
    this.modelVariant = data.modelVariant || "";
  }
}
