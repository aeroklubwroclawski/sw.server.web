import {action, autorun, observable, toJS} from 'mobx';
import {guid} from '../util';

export default class Turnpoint {
  id = "";
  @observable name = "";
  @observable type = "";
  @observable latitude = "";
  @observable longitude = "";
  @observable minAltitude = "";
  @observable maxAltitude = "";
  @observable observationZone = "";
  @observable radius = "";
  @observable trigger = "";
  @observable coneIncline = "";

  constructor() {
    this.id = guid();
    autorun(() => console.log(toJS(this)));
  }

  @action fromApi(data) {
    this.id = data.id;
    this.name = data.name;
    this.type = data.type;
    this.latitude = data.latitude.toString();
    this.longitude = data.longitude.toString();
    this.minAltitude = data.minAltitude.toString();
    this.maxAltitude = data.maxAltitude.toString();
    this.observationZone = data.observationZone;
    this.radius = data.radius.toString();
    this.trigger = data.trigger;
    this.coneIncline = data.coneIncline.toString();
  }
}
