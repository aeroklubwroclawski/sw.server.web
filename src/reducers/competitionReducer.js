import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function competitionReducer(state = initialState.competitions, action) {
  switch (action.type) {
    case types.LOAD_COMPETITIONS_SUCCESS:
      return {
        ...state,
        ...action.payload.entities.competitions
      };

    case types.CREATE_COMPETITION_SUCCESS:
      return {
        ...state,
        [action.competition.id]: action.competition
      };

    case types.UPDATE_COMPETITION_SUCCESS:
      return {
        ...state,
        [action.competition.id]: Object.assign({}, action.competition)
      };

    case types.DELETE_COMPETITION_SUCCESS:
      return Object.keys(state)
        .filter(key => key != action.competitionId)
        .reduce((obj, key) =>{
          obj[key] = state[key];
          return obj;
        }, {});

    default:
      return state;
  }
}
