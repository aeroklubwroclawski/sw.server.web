import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function eventReducer(state = initialState.events, action) {
  switch(action.type) {
    case types.LOAD_EVENTS_SUCCESS:
      return {
        ...state,
        ...action.payload.entities.events
      };

    default:
      return state;
  }
}
