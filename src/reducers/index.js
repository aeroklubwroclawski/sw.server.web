import {combineReducers} from 'redux';
import competitions from './competitionReducer';
import events from './eventReducer';

const rootReducer = combineReducers({
  competitions,
  events
});

export default rootReducer;
