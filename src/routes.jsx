import React from 'react';
import {Route} from 'react-router-dom';

import HomePage from './components/home/HomePage';
import CompetitionPage from './components/competition/CompetitionPage';
import ManageCompetitionPage from './components/competition/ManageCompetitionPage';
import CompetitionEventsPage from './components/event/CompetitionEventsPage';
import ManageEventPage from './components/event/ManageEventPage';

export default (
  <div>
    <Route path="/" exact component={HomePage}/>
    <Route path="/competitions" exact component={CompetitionPage}/>
    <Route path="/competition" exact component={ManageCompetitionPage}/>
    <Route path="/competition/:id" exact component={ManageCompetitionPage}/>
    <Route path="/competition/:id/events" exact component={CompetitionEventsPage}/>
    <Route path="/competition/:id/event" exact component={ManageEventPage}/>
    <Route path="/competition/:id/event/:eventId" exact component={ManageEventPage}/>
  </div>
);
