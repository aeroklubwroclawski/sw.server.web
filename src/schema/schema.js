import {schema} from 'normalizr';

export const turnpoint = new schema.Entity('turnpoints');

export const task = new schema.Entity('tasks');

export const event = new schema.Entity('events', {
  task: task
});

export const competition = new schema.Entity('competitions', {
  events: [event]
});

export const competitions = new schema.Array(competition);
