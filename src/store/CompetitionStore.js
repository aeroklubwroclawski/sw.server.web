import {action, observable} from 'mobx';
import Competition from '../model/Competition';
import CompetitionApi from '../api/competitionApi';

export default class CompetitionStore {
  @observable competitions = [];
  @observable isLoading = false;

  @action fetchAll() {
    this.isLoading = true;

    CompetitionApi
      .getAllCompetitions()
      .then(competitions => {
          competitions.map(c => {
              let value = new Competition();
              value.fromApi(c);
              this.competitions.push(value);
            }
          );
          this.isLoading = false;
        }
      );
  }

  @action delete(competition) {
    CompetitionApi.deleteCompetitionById(competition.id)
      .then(() => {
        this.competitions = this.competitions.splice(this.competitions.indexOf(competition), 1);
      });
  }
}
