import {action, observable} from 'mobx';
import Event from '../model/Event';
import EventApi from '../api/eventApi';

export default class EventStore {
  @observable events = [];
  @observable isLoading = false;

  @action fetchAll(competitionId) {
    this.isLoading = true;

    EventApi
      .getCompetitionEvents(competitionId)
      .then(events => {
          events.map(e => {
              let value = new Event();
              value.fromApi(e);
              this.events.push(value);
            }
          );
          this.isLoading = false;
        }
      );
  }

  @action deleteEvent(competitionId, event) {
    EventApi.deleteEvent(competitionId, event)
      .then(() => {
        const index = this.events.indexOf(event);
        if (index >= 0) {
          this.events.splice(index, 1);
        }
      });
  }
}
