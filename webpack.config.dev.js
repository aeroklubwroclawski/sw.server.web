"use strict";

let webpack = require("webpack");
let Path = require("path");
let loaders = require("./webpack.loaders");
let ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  entry: [
    "font-awesome-loader",
    "bootstrap-loader",
    "./src/index.jsx"
  ],

  target: 'web',

  output: {
    path: Path.resolve(__dirname, "dist"),
    filename: "app.js",
    publicPath: ""
  },

  devtool: "source-map",

  resolve: {
    alias: {
      jquery: 'jquery/src/jquery'
    },
    extensions: ["*", ".js", ".jsx", ".config.js", ".loaders.js"]
  },

  module: {
    loaders
  },

  devServer: {
    contentBase: "./src"
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new ExtractTextPlugin("styles.css")
  ]
};
