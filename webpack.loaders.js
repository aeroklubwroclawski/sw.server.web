"use strict";

var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = [
  {
    test: /\.jsx?$/,
    exclude: /node_modules/,
    loader: 'babel-loader',
    query: {
      presets: ['es2015', 'react', 'stage-2'],
      plugins: ['transform-decorators-legacy']
    }
  },
  {
    test: /\.css$/,
    loader: ExtractTextPlugin.extract({
      fallback: "style-loader", use: "css-loader"
    })
  },
  {
    test: /\.scss$/,
    loader: ExtractTextPlugin.extract({
      fallback: "style-loader",
      use: [
        "css-loader",
        "sass-loader",
        {
          loader: "sass-resources-loader",
          options: {
            resources: "./app/styles/**/*.scss"
          }
        }]
    })
  },
  {
    test: /bootstrap-sass[\/\\]assets[\/\\]javascripts[\/\\]/,
    use: 'imports-loader?jQuery=jquery'
  },
  {
    test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,
    loader: 'url-loader?name=fonts/[name].[ext]&limit=10000&mimetype=application/font-woff'
  },
  {
    test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
    loader: 'url-loader?name=fonts/[name].[ext]&limit=10000&mimetype=application/octet-stream'
  },
  {
    test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
    loader: 'file-loader?name=fonts/[name].[ext]'
  },
  {
    test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
    loader: 'url-loader?name=fonts/[name].[ext]&limit=10000&mimetype=image/svg+xml'
  }
];
